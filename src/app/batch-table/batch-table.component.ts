import { AfterContentInit, AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

export interface DataElement {
  id: number;               // run id or project index
  part_file: string;        // part file
  gate_location: string;    // gate location
  seeding_size?: number|string;    // mesh seed size (optional)
  part_material: string;    // part material
  hostname?: string;        // host name (optional)
  remark?: string;           // run remark
}

const TABLE_DATA : DataElement[] = [
  {
    id: 1,
    part_file: 'aaa.prt',
    gate_location: '(1.0, 2.0, 3.0)',
    part_material: 'abs.mtr',
    remark: 'test',
    seeding_size: ''
  },
  {
    id: 2,
    part_file: 'bbb.prt',
    gate_location: '(2.0, 3.0, 4.0)',
    part_material: 'abs2.mtr',
    remark: 'test2',
    seeding_size: ''
  }

];




@Component({
  selector: 'app-batch-table',
  templateUrl: './batch-table.component.html',
  styleUrls: ['./batch-table.component.css']
})
export class BatchTableComponent implements OnInit, AfterContentInit, AfterViewInit {

  @ViewChild('inb') inb! : ElementRef;
  @ViewChild('f') form! : NgForm;

  dataSource = TABLE_DATA;
  path:string = '';
  columnDefine : string[] = ['id', 'Part', 'Gate', 'Material', 'Hostname', 'Remark', 'Edit Run(s)', 'Delete'];

  constructor() { }

  ngOnInit(): void {
    this.path = 'ngOninit'



  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
  }

  ngAfterContentInit(): void {
    //Called after ngOnInit when the component's or directive's content has been initialized.
    //Add 'implements AfterContentInit' to the class.
  }

  submitForm(f:NgForm) {
    console.log(f);
  }

  setbyelem() {
    this.inb.nativeElement.value = 'by elem ref';
  }

  setbyform() {
    this.form.form.patchValue({
      inb: 'by form'
    })

  }

}
