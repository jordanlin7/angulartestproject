import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BatchTableComponent } from './batch-table/batch-table.component';
import { MatTableModule } from '@angular/material/table';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [AppComponent, BatchTableComponent],
  imports: [BrowserModule, BrowserAnimationsModule, MatTableModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
